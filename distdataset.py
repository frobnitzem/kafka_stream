#
# Consumer to load a data stream into https://github.com/ORNL/DDStore
#
from collections.abc import Iterable
from typing import Optional, Tuple
import logging
logger = logging.getLogger(__name__)

from mpi4py import MPI
import numpy as np

import torch
from torch.utils.data import Dataset

try:
    import pyddstore as dds
except ImportError:
    pass

def load_images(ddstore : dds.PyDDStore,
                key : str,
                loader : Iterable[np.ndarray]) -> Tuple[int, str, Tuple]:
    """ Consume all images from loader and place into the
        corresponding key in ddstore.

        Assumes each item returned from the loader
        is a (variably sized) batch of (uniformly sized) images.
        e.g.
        for images in loader:
            images.shape[0] == number of images in this batch
            images.shape[1:] == shape of each image

        Returns the total number of images loaded,
        and "dtype", (shape) -- if number > 0
            or "", () -- otherwise
    """
    n = 0
    images = None
    for images in loader:
        images = np.ascontiguousarray( images )
        self.ddstore.add(key, images)
        n += len(images)
    if images is None:
        return n, "", tuple()
    return n, images.dtype, images.shape[1:]

class DistDataset(Dataset):
    """Distributed dataset class"""

    def __init__(self,
                 loader : Iterable[np.ndarray],
                 comm = MPI.COMM_WORLD,
                 ddstore_width: Optional[int] = None) -> None:
        super().__init__()
        self.key = "data"
        
        self.loader = loader
        self.comm = comm
        self.rank = self.comm.Get_rank()
        self.comm_size = self.comm.Get_size()
        logger.debug("Initialized DistDataset: %d of %d", self.rank, self.comm_size)
        self.ddstore_width = (
            ddstore_width if ddstore_width is not None else self.comm_size
        )
        self.ddstore_comm = self.comm.Split(self.rank // self.ddstore_width, self.rank)
        self.ddstore_comm_rank = self.ddstore_comm.Get_rank()
        self.ddstore_comm_size = self.ddstore_comm.Get_size()
        self.ddstore = dds.PyDDStore(self.ddstore_comm)

        # TODO: start a background process to load data
        nimage, dtype, shape = load_images(self.ddstore, self.key, loader)

        # TODO: run this finalizer after process completes
        self.total_ns = self.comm.allreduce(nimage, MPI.SUM)
        assert self.total_ns > 0, "Error! no images loaded!"

        ok = self.rank if nimage > 0 else self.comm_size
        r1 = self.comm.allreduce(ok, MPI.MIN)
        dtype, shape = comm.bcast( (dtype, shape), root = r1)
        self.dtype = dtype
        self.shape = shape
        if self.rank == 0:
            logger.info("Loaded %d images. dtype = %s, shape = %s",
                        self.total_ns, self.dtype, str(self.shape))

    def len(self):
        return self.total_ns

    def __len__(self):
        return self.len()

    def get(self, idx):
        val = np.zeros(self.shape, dtype=self.dtype)
        assert val.data.contiguous
        self.ddstore.get(self.key, val, idx)
        return torch.tensor(val)

    def __getitem__(self, idx):
        return self.get(idx)
