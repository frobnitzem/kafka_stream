from collections.abc import Iterator
import time

import numpy as np

from kafka import KafkaConsumer

# https://www.conduktor.io/kafka/kafka-consumer-groups-and-consumer-offsets/
from kafka import TopicPartition, OffsetAndMetadata

from serial import deserialize

def pull_data(consumer, ndone) -> Iterator[np.ndarray]:
    """ Consider this pull operation complete after
        receiving at least ndone images.
    """
    nmsg = 0
    if ndone == 0:
        raise StopIteration
    for message in consumer:
        yield message
        nmsg += 1
        if nmsg >= ndone:
            break

def consume(argv):
    assert len(argv) == 2, f"Usage: {argv[0]} <nmsg>"

    consumer = KafkaConsumer(
        "image-test",
        bootstrap_servers="localhost:19092",
        group_id="learner",
        value_deserializer = deserialize,
        auto_offset_reset="latest",
        enable_auto_commit=True,
    )
    nmsg = int(argv[1])

    n = 0
    nbyte = 0
    start = time.time()
    message = None
    try:
        for message in pull_data(consumer, nmsg):
            x = message.value
            n += len(x)
            nbyte += x.nbytes
            print(message.partition, message.offset, message.key, x.shape)
    except KeyboardInterrupt:
      pass

    # if message:
    #     consumer.commit({TopicPartition(message.topic, message.partition):
    #                      OffsetAndMetadata(message.offset + 1, '')})
    end = time.time()
    t = end-start
    print(f"Read {n} images in {t} seconds: {nbyte/t/1024**2} MB/sec.")

if __name__ == "__main__":
    import sys
    consume(sys.argv)
