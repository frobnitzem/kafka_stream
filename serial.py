import io
import numpy as np

# Messages will be serialized as npy file format
def serialize(x : np.ndarray) -> bytes:
    #return json.dumps(x).encode('utf-8')
    #with io.BytesIO() as f:
    f = io.BytesIO()
    np.save(f, x, allow_pickle=False)
    #return f.getbuffer()
    return f.getvalue()

def deserialize(b : bytes) -> np.ndarray:
    with io.BytesIO(b) as f:
        return np.load(f, allow_pickle=False)

def test_serialize():
    for shape in [(10,), (32,32), (1,1024,1024)]:
        x = np.random.random( shape )
        b = serialize(x)
        y = deserialize(b)
        assert x.shape == y.shape
        assert np.allclose(x, y)
