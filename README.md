# Kafka Streaming Data Demo

Test out streaming bandwidth experienced
by Kafka in python.

David M. Rogers
June 4, 2024

| :warning: This project is superceded by [NNG Stream](https://gitlab.com/frobnitzem/nng_stream) |
|-------------------------------------|


Created following [confluent's getting started](https://developer.confluent.io/get-started/python).

# Design

The Kafka-ML project shows a working example of utilizing Kafka
data streams for online training of ML models.
It is described in [this paper](https://doi.org/10.1016/j.future.2021.07.037).

The basic Dataset (consumer) class is [here](https://github.com/ertis-research/kafka-ml/blob/1cdb7f405f29a985f78f522433399f36a36bda8e/model_training/pytorch/TrainingKafkaDataset.py)

and the data source (producer) is [here](https://github.com/ertis-research/kafka-ml/blob/1cdb7f405f29a985f78f522433399f36a36bda8e/datasources/raw_sink.py).
The naming convention has the producer sending messages to
a "sink" class.  That sink (acting as the network producer)
routes the messages through the Kafka
brokers.  Data consumers subscribe to the appropriate topics
and begin receiving forwarded messages from the brokers.

# Demo Code

After setting up a cluster of 3 Kafka brokers following `setup_kafka.rc`,
we can see 3 brokers and a zookeeper running in containers:
```
% podman container ls
CONTAINER ID  IMAGE                                              COMMAND               CREATED            STATUS                PORTS       NAMES
cc85d4edfdb0  docker.io/confluentinc/cp-zookeeper:latest         /etc/confluent/do...  About an hour ago  Up About an hour ago              enterprise-kafka_zookeeper_1
32f0c84a1840  docker.io/confluentinc/cp-enterprise-kafka:latest  /etc/confluent/do...  About an hour ago  Up About an hour ago              enterprise-kafka_kafka-1_1
a53bcd7c1a70  docker.io/confluentinc/cp-enterprise-kafka:latest  /etc/confluent/do...  About an hour ago  Up About an hour ago              enterprise-kafka_kafka-2_1
4395b7c889f9  docker.io/confluentinc/cp-enterprise-kafka:latest  /etc/confluent/do...  About an hour ago  Up About an hour ago              enterprise-kafka_kafka-3_1
```

Next, the consumer and producer code was run, demonstrating the bandwidth
obtainable at each end,
```
kafka-demo (ai)% python3 producer.py 1024 32
Sent 32 messages in 0.38094401359558105 seconds: 672.0147603415958 MB/sec.
kafka-demo (ai)% python3 producer.py 1024 32
Sent 32 messages in 0.28516197204589844 seconds: 897.7354103821226 MB/sec.
kafka-demo (ai)% python3 producer.py 128 32
Sent 32 messages in 0.33455634117126465 seconds: 11.956132668106676 MB/sec.
kafka-demo (ai)% python3 producer.py 128 1024
Sent 1024 messages in 8.37088680267334 seconds: 15.29109197356745 MB/sec.
kafka-demo (ai)% python3 producer.py 32 10
Sent 10 messages in 0.02125859260559082 seconds: 3.6749845791510123 MB/sec.
kafka-demo (ai)% python3 producer.py 32 1024
Sent 1024 messages in 0.6189658641815186 seconds: 12.924783841801512 MB/sec.
```

The first two tests created messages of size 8MB each, which were silently
dropped.  Their high data rate was because those messages were never delivered.
The message rate is slightly higher on sending more and larger data.
A speed of 13 MB/sec is obtainable using 32^2 images of float64-s.

At the consumer side, the message rate is slower,
```
kafka-demo (ai)% python3 consumer.py 1024
19 2 None (1, 1, 128, 128)
19 3 None (1, 1, 128, 128)
10 0 None (1, 1, 128, 128)
10 1 None (1, 1, 128, 128)
13 0 None (1, 1, 128, 128)
...
Read 1024 images in 28.770556449890137 seconds: 4.448992852221625 MB/sec.
```
Repeating the test, however, shows that some messages are replayed.
```
kafka-demo (ai)% python3 consumer.py 1024
4 54 None (1, 1, 128, 128)
4 55 None (1, 1, 128, 128)
4 56 None (1, 1, 128, 128)
4 57 None (1, 1, 128, 128)
4 58 None (1, 1, 128, 128)
4 59 None (1, 1, 128, 128)
4 60 None (1, 1, 128, 128)
...
^CRead 76 images in 37.91175723075867 seconds: 0.2505818958001882 MB/sec.
```
This is likely due to the windowing behavior of Kafka, and requires
careful configuration.

It is possible that cluster configuration could accelerate these
rates significantly.  In particular, disabling caching to disk
and message redundancy.

# Steps to Test in Actual Use Case

The infrastructure described above could be deployed and tested
on current OLCF hardware using the following steps:

1. deploy kafka zookeeper and brokers to `*`-lrn044.apps.olivine.ccs.ornl.gov
2. run a data producer on summit sending data from psana
3. run a data consumer on defiant reading data from olivine

The `distdataset.py` program provides an example data consumer
that loads all data into
[DDStore](https://dl.acm.org/doi/fullHtml/10.1145/3624062.3624171).
It is intended to be run from all ranks in an MPI-parallel program.
Each rank loops over an iterable whose items are sourced by consuming
input from the data stream.  To prevent overwhelming the sender
with connection requests, it is advisable to setup
some of the ranks with an empty iterable.
