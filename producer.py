# Basic producer sending images over a Kafka
# stream.
from collections.abc import Iterable
import time 
import json 
import random 
from datetime import datetime

import numpy as np

from kafka import KafkaProducer

from data_generator import image_generator
from serial import serialize

def send_data(producer : KafkaProducer,
              topic : str,
              gen : Iterable[np.ndarray]) -> None:
    # Send all generator output to the Kafka topic.
    n = 0
    nbyte = 0
    start = time.time()
    for image in gen:
        producer.send(topic, image)
        n += 1
        nbyte += image.nbytes
    end = time.time()
    t = end-start
    print(f"Sent {n} messages in {t} seconds: {nbyte/t/1024**2} MB/sec.")

def produce(argv):
    assert len(argv) == 3, f"Usage: {argv[0]} <image dim> <number of images>"
    producer = KafkaProducer(
        bootstrap_servers=['localhost:19092'],
        value_serializer=serialize
    )
    dim = int(argv[1])
    nmsg = int(argv[2])

    gen = image_generator((1,1,dim,dim), nmsg) # 1-channel dim^2 images
    send_data(producer, "image-test", gen)
    producer.flush()

if __name__ == '__main__':
    import sys
    produce(sys.argv)
